<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \app\models\Instruction $model */

?>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Редактирование инструкции</h4>
    </div>
    <div class="panel-body">
        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'content')->widget(\mihaildev\ckeditor\CKEditor::class, [

        ])->label('Контент') ?>

            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
