<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Logs */

?>
<div class="logs-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
