<?php
use app\models\Logs;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Зафиксировано',
        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
        'width' => '10%',
        'filterWidgetOptions' => [
            'convertFormat' => true,
            'pluginOptions' => [
                'timePicker'=>true,
                'timePickerIncrement'=>30,
                'timePicker24Hour' => true,
                'locale' => [
                    'format' => 'Y-m-d H:i:s',
                ],
            ],
        ],
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.name',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => 'Выберите пользователя',
            ],
        ],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'event',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterWidgetOptions' => [
            'data' => Logs::logLabels(),
            'pluginOptions' => [
                'allowClear' => true,
                'placeholder' => 'Выберите тип',
            ],
        ],
        'content' => function($model) {
            if($model->event != null && isset(Logs::logLabels()[$model->event]))
                return Logs::logLabels()[$model->event];
            else
                return null;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'text',
        'format' => 'raw'
    ],

];   