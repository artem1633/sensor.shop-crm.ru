<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Рабочий стол', 'icon' => 'fa fa-laptop', 'url' => ['/dashboard'],],
                    ['label' => 'Настройки', 'icon' => 'fa fa-th-large', 'url' => ['#'], 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'], 'visible' => Yii::$app->user->identity->role === \app\models\User::ROLE_ADMIN],
                        ['label' => 'Устройства', 'icon' => 'fa fa-bolt', 'url' => ['/devices']],
                        ['label' => 'Логи', 'icon' => 'fa fa-pencil', 'url' => ['/logs'], 'visible' => Yii::$app->user->identity->role === \app\models\User::ROLE_ADMIN],
                        ['label' => 'Редактор инструкции', 'icon' => 'fa fa-plus', 'url' => ['/instruction/edit'], 'visible' => Yii::$app->user->identity->role === \app\models\User::ROLE_ADMIN],
                    ]],
                    ['label' => 'Монитор', 'icon' => 'fa fa-area-chart', 'url' => ['/monitor'],],
                    ['label' => 'Архив', 'icon' => 'fa fa-calendar', 'url' => ['/archive'],],
                    ['label' => 'Экспорт', 'icon' => 'fa fa-file-excel-o', 'url' => ['/gases-values'],],
                    ['label' => 'Автосохранение', 'icon' => 'fa fa-file-excel-o', 'url' => ['/export-autosave'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
