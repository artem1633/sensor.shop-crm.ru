<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GasesValues */
?>
<div class="gases-values-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'device_id',
            'CO',
            'NO',
            'NO2',
            'SO2',
            'created_at',
        ],
    ]) ?>

</div>
