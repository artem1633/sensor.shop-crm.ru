<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExportAutosave */
/* @var $form yii\widgets\ActiveForm */

if($model->gases != null && !is_array($model->gases))
    $model->gases = explode(',', $model->gases);

if($model->device_id != null && !is_array($model->device_id))
    $model->device_id = explode(',', $model->device_id);

?>

<div class="export-autosave-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'device_id')->widget(kartik\select2\Select2::class, [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Devices::find()->all(), 'id', 'name'),
        'options' => ['multiple' => true],
    ]) ?>

    <?= $form->field($model, 'gases')->widget(kartik\select2\Select2::class, [
        'options' => ['multiple' => true],
    ])->label('Газы') ?>

    <?= $form->field($model, 'folder_name')->textInput() ?>

    <?= $form->field($model, 'interval')->input('number') ?>

    <?= $form->field($model, 'date_range')->widget(kartik\daterange\DateRangePicker::class, [
        'convertFormat' => true,
        'pluginOptions' => [
            'locale' => [
                'format' => 'Y-m-d',
            ],
        ],
    ])?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
    $('#exportautosave-device_id').change(function()
    {
       var val = $(this).val();
       
       console.log(val.join(','));
       
       $.get('/devices/get-gases?id='+val.join(','), function(response){
             $('#exportautosave-gases').html('').trigger('change'); 
             $.each(response, function(i, key){
                  $('#exportautosave-gases').append(new Option(i, key, false, false)).trigger('change');
             });
       });
       
    });

    $('#exportautosave-device_id').trigger('change');

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>