<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */


$model->accessDevices = array_values(ArrayHelper::map($model->devices,'id', 'id'));

?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->dropDownList(\app\models\User::getRolesLabels(), ['prompt' => 'Выберите роль']) ?>

    <div id="access-devices-wrapper">
        <?= $form->field($model, 'accessDevices')->widget(kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Devices::find()->all(), 'id', 'name'),
            'options' => [
                'multiple' => true,
            ],
            'defaultOptions' => ['class' => 'hidden'],
        ]) ?>
    </div>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip',])->hint('
        Пароль должен содержать:
        <ul>
            <li>только латинские буквы</li>
            <li>минимум 6 символов</li>
            <li>миним одну заглавную букву</li>
            <li>миним одну цифру</li>
        </ul>
    ') ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$script = <<< JS
    if($('#user-role').val() == 'DEFAULT')
    {
       $('#access-devices-wrapper').show(); 
    } else {
        $('#access-devices-wrapper').hide();
    }
    
    $('#user-role').change(function(){
        if($(this).val() == 'DEFAULT')
        {
            $('#access-devices-wrapper').show();
        } else {
            $('#access-devices-wrapper').hide();
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>