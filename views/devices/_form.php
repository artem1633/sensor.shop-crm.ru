<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Devices */
/* @var $form yii\widgets\ActiveForm */

if($model->gases != null && !is_array($model->gases))
    $model->gases = explode(',', $model->gases);

?>

<div class="devices-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->role != \app\models\User::ROLE_DEFAULT): ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <div class="row">
            <div class="col-md-9">
                <?= $form->field($model, 'ip')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <button id="load-params-btn" class="btn btn-primary btn-sm" style="margin-top: 24px;">Получить значения</button>
            </div>
        </div>

        <?= $form->field($model, 'gases')->widget(Select2::class, [
            'id' => 'gases-select2',
            'options' => ['multiple' => true],
        ]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'connection_time')->input('number', ['min' => 0]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'is_in_dashboard')->checkbox() ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>

</div>

<?php

$script = <<< JS
    $('#load-params-btn').click(function(e){
        e.preventDefault();
        var val = $('#devices-ip').val();
        
        if(val != '')
        {
            $.get('/devices/ip-params?ip='+val, function(response){
                console.log(response);
                    $('#devices-gases').html('').trigger('change'); 
                    $.each(response, function(i, key){
                        $('#devices-gases').append(new Option(i, key, false, false)).trigger('change');
                    });
            });
        }
    });
JS;


$this->registerJs($script, \yii\web\View::POS_READY);


if($model->isNewRecord == false)
{
    $script = <<< JS
        var val = $('#devices-ip').val();
        
        if(val != '')
        {
            $.get('/devices/ip-params?ip='+val, function(response){
                console.log(response);
                    $('#devices-gases').html('').trigger('change'); 
                    $.each(response, function(i, key){
                        $('#devices-gases').append(new Option(i, key, false, false)).trigger('change');
                    });
            });
        }
JS;

    $this->registerJs($script, \yii\web\View::POS_READY);

}

?>