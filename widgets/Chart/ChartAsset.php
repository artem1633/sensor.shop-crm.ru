<?php

namespace app\widgets\Chart;

use yii\web\AssetBundle;

class ChartAsset extends AssetBundle
{
    public $sourcePath = 'theme/assets/plugins';

    public $basePath = '@web';

    public $css = [
        'nvd3/build/nv.d3.css',
    ];

    public $js = [
        'flot/jquery.flot.min.js',
        'flot/jquery.resize.js',
        'flot/jquery.pie.js',
        'flot/jquery.navigate.js',
        'flot/jquery.flot.time.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
        'app\assets\ColorAdminAsset'
    ];
}