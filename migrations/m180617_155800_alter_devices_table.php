<?php

use yii\db\Migration;

/**
 * Class m180617_155800_alter_devices_table
 */
class m180617_155800_alter_devices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('devices', 'name', $this->string()->after('id')->comment('Наименование устройства'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('devices', 'name');
    }
}
