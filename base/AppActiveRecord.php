<?php

namespace app\base;

use yii\db\ActiveRecord;

/**
 * Class AppActiveRecord
 * @package app\base
 */
abstract class AppActiveRecord extends ActiveRecord
{
    /**
     * @return string
     */
    public function getEntityName()
    {
        return self::class;
    }

    /**
     * @return string
     */
    abstract function getViewUrl();
}