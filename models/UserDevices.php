<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_devices".
 *
 * @property int $user_id
 * @property int $devices_id
 *
 * @property Devices $devices
 * @property User $user
 */
class UserDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'devices_id'], 'required'],
            [['user_id', 'devices_id'], 'integer'],
            [['user_id', 'devices_id'], 'unique', 'targetAttribute' => ['user_id', 'devices_id']],
            [['devices_id'], 'exist', 'skipOnError' => true, 'targetClass' => Devices::className(), 'targetAttribute' => ['devices_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'devices_id' => 'Devices ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasOne(Devices::className(), ['id' => 'devices_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
